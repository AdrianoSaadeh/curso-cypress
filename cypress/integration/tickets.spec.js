describe('Tickets', () => {

    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("fills all the text input fields", () => {
        const firstName = "Adriano";
        const lastName = "Saadeh";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("adrianosaadeh@gmail.com");
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("Select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("Select 'vip' ticket type", () => {
        cy.get("#vip").check();
    });

    it("Select 'social media' checkbox", () => {
        cy.get("#social-media").check();
    });

    it("Selects 'friend', and 'publication', then uncheck 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("alerts on invalid email", () => {
        cy.get("#email")
          .as("email")
          .type("emailinvalido.com.br");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
          .clear()
          .type("emailvalido@com.br");

        cy.get("#email.invalid").should("not.exist");
    });

    it("fills and reset the form", () => {
        const firstName = "Adriano";
        const lastName = "Saadeh";
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("adrianosaadeh@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA Beer");

        cy.get(".agreement p").should("contain", `${fullName}, wish to buy 2 VIP tickets.`);

        cy.get('#agree').click();
        cy.get('#signature').type(fullName);

        cy.get('[type="submit"]').as("submitButton").should("not.be.disabled");

        cy.get('.reset').click();

        cy.get("@submitButton").should("be.disabled");
    });

    it("fills mandatory fields using support comand", () =>  {
        const customer = {
            firstName: "Joao",
            lastName: "Silva",
            email: "joaosilva@gmail.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get('[type="submit"]').as("submitButton").should("not.be.disabled");
        cy.get('#agree').uncheck();
        cy.get("@submitButton").should("be.disabled");

    });

});

